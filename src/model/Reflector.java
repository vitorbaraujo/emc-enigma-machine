package model;

public class Reflector {
	public static final char[] set_A = { 'A', 'E', 'B', 'J', 'C', 'M', 'D', 'Z', 'F', 'L', 'G', 'Y', 'H', 'X', 'I', 'V', 'K', 'W', 'N', 'R', 'O', 'Q', 'P', 'U', 'S', 'T' };
	public static final char[] set_B = { 'A', 'Y', 'B', 'R', 'C', 'U', 'D', 'H', 'E', 'Q', 'F', 'S', 'G', 'L', 'I', 'P', 'J', 'X', 'K', 'N', 'M', 'O', 'T', 'Z', 'V', 'W' };
	public static final char[] set_C = { 'A', 'F', 'B', 'V', 'C', 'P', 'D', 'J', 'E', 'I', 'G', 'O', 'H', 'Y', 'K', 'R', 'L', 'Z', 'M', 'X', 'N', 'W', 'Q', 'T', 'S', 'U' };
	
	private char[] refSetup;
	
	public Reflector(ReflectorType refType){
		switch (refType){
			case A:
				refSetup = set_A;
				break;
			case B:
				refSetup = set_B;
				break;
			case C:
				refSetup = set_C;
				break;
			default:
				break;		
		}		
	}
	
	public char reflectChar(char _char){
		for (int i = 0; i < refSetup.length; i = i + 2){
			char charA = refSetup[i];
			char charB = refSetup[i + 1];
			
			if (_char == charA){
				return charB;
			}
			if (_char == charB){
				return charA;
			}
		}
		
		return _char;
	}
}
