<h1><strong>Trabalho Final - EnigmaMachine</strong></h1>

<p><strong>Alunos:</strong></p>

<ul>
<li>		Mateus Manuel Rodrigues Bezerra	- 14/0063978</li>
<li>		Vítor Barbosa de Araujo - 14/0033149</li>
</ul>

O projeto consiste em uma interface gráfica representada por
uma janela com botões, caixas de texto e pequenas caixas de
contagem.

O projeto foi elaborado utilizando a IDE Eclipse. Para executar
o programa, pode-se apenas executar o arquivo EnigmaMachine.jar,
localizado na pasta enviada ou adicionar o projeto ao workspace
do Eclipse e executá-lo como 'Java Application'.
